package activity07;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for ShiftedTalker class.
 */
public class ShiftedTalkerTest 
{
    @Test
    public void checkTalk() {
        ShiftedTalker st = new ShiftedTalker(3);

        assertEquals("defabc", st.talk("abcdef"));
    }
}
