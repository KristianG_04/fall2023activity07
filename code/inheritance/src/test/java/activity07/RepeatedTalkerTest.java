package activity07;

import org.junit.Test;

import static org.junit.Assert.*;

public class RepeatedTalkerTest {

    @Test
    public void testTalk(){
        RepeatTalker rt = new RepeatTalker(3);
        String greetings = "hello hello hello";
        assertEquals(greetings, rt.talk("hello"));

    }
    
}
