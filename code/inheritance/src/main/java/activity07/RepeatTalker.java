package activity07;

public class RepeatTalker extends Talker {
    private int n;

    public RepeatTalker(int n){
        this.n = n;
    }

    @Override
    public String talk(String s) {
        String newString = "";

        for (int i = 0; i < this.n; i++){
            newString += s;

            //Prevents a space from being added at the very end of the string
            if (i != this.n - 1){
                newString += " ";
            }
        }

        return newString;
    }
}
