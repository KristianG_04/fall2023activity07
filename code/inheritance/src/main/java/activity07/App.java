package activity07;

import java.util.Scanner;

public class App {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        
        System.out.print("Enter message:");
        String input = reader.next();
        Talker[] talkers = parse(input);

        print(talkers, "abcdef");
    }

    public static void print(Talker[] talkers, String message){
        for (int i = 0; i < talkers.length; i++){
            System.out.println(talkers[i].talk(message));
        }

    }

    public static Talker[] parse(String n){
        String[] format = n.split(";");
        Talker[] talkers = new Talker[format.length / 2];

        for (int i = 0; i < talkers.length; i++){
            if (format[(i * 2)].equalsIgnoreCase("repeattalker")){
                talkers[i] = new RepeatTalker(Integer.parseInt(format[(i * 2) + 1]));
            }
            else if (format[(i * 2)].equalsIgnoreCase("shiftedtalker")){
                talkers[i] = new ShiftedTalker(Integer.parseInt(format[(i * 2) + 1]));
            }
        }

        return talkers;
    }
}